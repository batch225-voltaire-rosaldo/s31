//Node.js Introduction

// Use the "require" directive load Node.js modules
// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol(HTTP)
// HTTP is a protocol that allows the fetching of resources such as HTML documents.
// The message sent by the "client", usually a Web browser, called "request"
// The messahe sent by the "server" as an answers are called "responses"

let http = require("http");

// // Using this module's createServer() method, we can create an HTTP server that listen to request on a specified port and gives reponses back to client.
// // A port is a virtual point where network connection start and end. Each port is associated with a specific process or server

// http.createServer(function (request, response) {

// 	//Use the writeHead() method to:
// 	//Set a status code for the response - a 200 means OK
// 	//Set the content-type of the response as a plain text message

// 	response.writeHead(200, {'Content-Type' : 'text/plain'});

// 	// send the response with text content:
// 	response.end("Welcome to my Page");

// 	//The server will be assigned to port 4000 via the "listen(4000)" method where server will listen to any request that are sent to it eventually communicating with our server.

// }).listen(4000)

// // when 
// console.log('Server is running at localhost:4000');


//========================
// Part 2 with condition

const server = http.createServer((request, response) => {

	if (request.url == '/greeting') {

		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end("Welcome to BPI");

	} else if (request.url == '/customer-service') {
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end("Welcome to Customer Service");
	} else {

		response.writeHead(404, { 'Content-Type': 'text/plain' });
		response.end("Page is not available");
	}

}).listen(4000);

console.log('Server running at localhost:4000');





































