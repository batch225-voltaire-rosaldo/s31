let http = require("http");

const server = http.createServer((request, response) => {

	if (request.url == '/login') {

		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.end("Welcome to the login page");

	// } else if (request.url == '/customer-service') {
	// 	response.writeHead(200, { 'Content-Type': 'text/plain' });
	// 	response.end("Welcome to Customer Service");
	} else {

		response.writeHead(404, { 'Content-Type': 'text/plain' });
		response.end("Page is not available");
	}

}).listen(3000);

console.log('Server running at localhost:3000');